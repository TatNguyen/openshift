package com.example.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ProjectApplication {

	@GetMapping("/")
	public String welcome() {
		return "Welcome to openshift";
	}

	@GetMapping("/{input}")
	public String congrat(@PathVariable String input) {
		return "Hello " + input + "! Deploy successfully!";
	}

	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class, args);
	}

}
